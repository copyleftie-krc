# copyleftie's K&R

I'm working through the exercises in the second edition of K&R C (*K&R2e*).
I'll be blogging my way through each chapter of the book as I complete it.

## repository structure

```
/copyleftie-krc
	|- LICENSE
	|- README.md
	|- chXX/
	|-	|- exXX-XX.{c,md}
```

The responses are contained in folders `ch01`, `ch02`, and so on, and the
responses themselves are normally named
`ex${chapter_number}-${exercise_number}.c`, with occasional exceptions for
written responses (`*.md`), variants (`ex*-${variant_name}.c`), or (rarely)
outright omissions (like exercise `1-1`).

### (where are the files?...)

Click `tree` in the above links.

## style

The code in this repository arbitrarily adopts [OpenBSD's
variant](https://man.openbsd.org/style) of [Kernel Normal Form
(KNF)](https://en.wikipedia.org/wiki/Kernel_Normal_Form), even where that style
explicitly contradicts *K&R2e*'s recommendations. This is an exercise in
"pick[ing] a style and stick[ing] with it", as recommended by the book itself.

## tcc

C files in this repository begin with a [`tcc`](http://repo.or.cz/w/tinycc.git)
shebang and are executable. `tcc` compiles fast enough to make C usable as a
scripting language. I'm not sure I'd recommend using C for such a purpose, but
executable `.c`s are very convenient for testing small programs like these ones.
## bugs

I would appreciate [bug reports](mailto:copyleftie@protonmail.com) if you
notice mistakes or have any suggestions.

Deviations from OpenBSD's KNF should be considered bugs.
