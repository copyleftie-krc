#!/usr/bin/tcc -run
#include <stdio.h>

#define LOWER (5.0/9.0 *   (0.0-32.0))
#define UPPER (5.0/9.0 * (300.0-32.0))
#define STEP  (5.0/9.0 * 20.0)

int
main() {
	float c;

	printf("%5s %6s\n", "C", "F");
	printf("============\n");

	/* floating point arithmetic is not quite exact, so buffer the limit */
	for (c = LOWER; c <= (UPPER + 0.1); c += STEP)
		printf("%5.1f %6.0f\n", c, (9.0/5.0*c) + 32.0);

	return 0;
}
