#!/usr/bin/tcc -run
#include <stdio.h>
#define TABSTOP 8

int
main() {
	int i, to_tab;
	char c;

	to_tab = 0;
	while ((c = getchar()) != EOF) {
		if (c == '\t') {
			for (i = 0; i < TABSTOP-to_tab; ++(i))
				putchar(' ');

			to_tab = 0;
		} else {
			if (c == '\n' || to_tab == TABSTOP-1)
				to_tab = 0;
			else
				++(to_tab);

			putchar(c);
		}
	}

	return 0;
}
