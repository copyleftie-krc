#!/usr/bin/tcc -run
#include <stdio.h>
#define MAXLINE 1024

int	getln(char[], int);
void	copy(char[], char[]);

int
main() {
	int len, full_len, max, wrap;
	char line[MAXLINE];
	char longest[MAXLINE];

	wrap = 0;
	max = 0;
	while ((len = getln(line, MAXLINE)) > 0) {
		full_len = len + wrap*MAXLINE;

		if (full_len > max) {
			max = full_len;
			if (wrap == 0)
				copy(longest, line);
		}

		if (line[len-1] != '\n')
			wrap += 1;
		else
			wrap = 0;
	}

	/* unsure if I prefer this to printing a tag string that can be empty */
	if (max > 0)
		if (max > MAXLINE-1)
			printf("%d: %s (truncated)", max, longest);
		else
			printf("%d: %s", max, longest);

	return 0;
}

/* Store the contents of the next line from stdin into the given string. Returns
 * the length of the line. */
int
getln(char s[], int lim) {
	int c, i;

	for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++(i))
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		++(i);
	}
	s[i] = '\0';

	return i;
}

/* Copy the contents of the first string into the second. The second string
 * must be long enough. */
void
copy(char to[], char from[]) {
	int i;

	i = 0;
	while ((to[i] = from[i]) != '\0')
		++(i);
}
