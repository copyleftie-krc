#!/usr/bin/tcc -run
#include <stdio.h>

#define MAX_CHAR 128
#define IN         1
#define OUT        0

int
main() {
	int c, i, state;
	int count[MAX_CHAR];

	for (i = 0; i < MAX_CHAR; ++(i))
		count[i] = 0;

	while ((c = getchar()) != EOF) {
		if (c < MAX_CHAR)
			++(count[c]);
	}

	for (c = 0; c < MAX_CHAR; ++(c)) {
		if (count[c] > 0) {
			if (c == '\n')
				printf("\\n");
			else if (c == '\b')
				printf("\\b");
			else if (c == '\t')
				printf("\\t");
			else if (c == '\\')
				printf("\\\\");
			else
				printf("%2c", c);
			putchar(' ');

			for (i = 0; i < count[c]; ++(i))
				putchar('*');
			putchar('\n');
		}
	}

	return 0;
}
