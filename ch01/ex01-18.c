#!/usr/bin/tcc -run
#include <stdio.h>
#define MAXLINE 1024

#define IN_NONBLANK 0
#define IN_BLANK    1
#define ZERO        0
#define NONZERO     1

int	insert(char[], char, int);
int	flush(char[], int);
int	empty(char[]);

int
main() {
	int i, len, state;
	char buffer[MAXLINE], c;

	i = 0;
	len = ZERO;
	state = IN_NONBLANK;
	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\t') {
			state = IN_BLANK;

			i = insert(buffer, c, i);
		} else {
			if (c == '\n') {
				if (len == NONZERO)
					putchar(c);

				i = empty(buffer);
				len = ZERO;
			} else {
				if (state == IN_BLANK) {
					i = flush(buffer, i);
					state = IN_NONBLANK;
				}

				if (len == ZERO)
					len = NONZERO;

				putchar(c);
			}
		}
	}

	return 0;
}

/* Store the given character into the given character buffer at the given index.
 * Returns the next index to store a character into. */
/* TODO: likely data loss at 1024 consecutive blanks */
int
insert(char s[], char c, int i) {
	if (i == MAXLINE-1) {
		return empty(s);
	} else {
		s[i] = c;
		return i+1;
	}
}

/* Print the given string. Always returns 0 (the new index of the string being
 * flushed). */
int
flush(char s[], int i) {
	s[i] = '\0';
	printf("%s", s);

	return 0;
}

/* Empty the given string. Always returns 0 (the new index of the string being
 * flushed) */
int
empty(char s[]) {
	s[0] = '\0';

	return 0;
}

