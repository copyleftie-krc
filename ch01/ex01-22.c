#!/usr/bin/tcc -run
#include <stdio.h>

#define LINEWRAP 80

void	flush(char[], int);

/* Exercise 1-22
 *
 * Wrap stdin to LINEWRAP columns, breaking lines at whitespace. Scans the input
 * by character, each time asking three questions in sequence:
 *
 * (1) Is this the start of a new line?
 * (2) Is the word-buffer being flushed, or appened to?
 * (3) Does the position tracker need to be updated?
 *
 * This assumes all characters take one space (tabs should already have been
 * expanded) as well as that the user is fine with words longer than LINEWRAP
 * not being wrapped. It also uses `wc`'s definition of word, and does not wrap
 * at slashes, hyphens, HTML tag boundaries, or the like. */
int
main() {
	int c, len, pos; 
	char word[LINEWRAP+1];

	/* pos represents the position of the previously printed char. */
	pos = len = 0;
	while ((c = getchar()) != EOF) {
		if (c == '\n' || pos >= LINEWRAP && c == ' ') {
			if (c != '\n')
				putchar('\n');

			pos = len;
		}

		if (c == '\n' || c == ' ') {
			flush(word, len);
			putchar(c);

			len = 0;
		} else {
			word[len] = c;

			++(len);
		}

		if (c != '\n')
			++(pos);
	}

	return 0;
}

/* Print the given string, whose length should be known at call time. */
void
flush(char s[], int l) {
	s[l] = '\0';
	printf("%s", s);
}

