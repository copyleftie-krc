#!/usr/bin/tcc -run
#include <stdio.h>

#define DEFAULT   0
#define MAYBE     1
#define QUOTE     2
#define COMMENT   3
#define MAYBE_DEF 4

int
main() {
	int c, state;
	char qmark;

	state = DEFAULT;
	while ((c = getchar()) != EOF) {
		if ((state == DEFAULT || state == MAYBE)
		    && (c == '\'' || c == '\"'))
		{
			state = QUOTE;
			qmark = c;

			putchar(c);
		} else if (state == DEFAULT) {
			if (c == '/')
				state = MAYBE;
			else
				putchar(c);
		} else if (state == MAYBE) {
			if (c == '*')
				state = COMMENT;
			else {
				state = DEFAULT;
				printf("/%c", c);
			}
		} else if (state == QUOTE) {
			if (c == qmark)
				state = DEFAULT;

			putchar(c);
		} else if (state == COMMENT) {
			if (c == '*')
				state = MAYBE_DEF;
		} else if (state == MAYBE_DEF) {
			if (c == '/')
				state = DEFAULT;
			else
				state = COMMENT;
		}
	}
	
	return 0;
}
