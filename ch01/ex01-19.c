#!/usr/bin/tcc -run
#include <stdio.h>
#define MAXLINE 1024

int	 getln(char[], int);
char	*reverse(char[]);
void	 strip(char[]);
int	 strln(char[]);

int
main() {
	int len;
	char line[MAXLINE];

	while ((len = getln(line, MAXLINE)) > 0)
		printf("%s", reverse(line));

	return 0;
}

/* Store the contents of the next line from stdin into the given string. Returns
 * the length of the line. */
int
getln(char s[], int lim) {
	int c, i;

	for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++(i))
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		++(i);
	}
	s[i] = '\0';

	return i;
}

/* Reverse the given string in place.
 * Returns a pointer to that string. */
char[]
reverse(char s[]) {
	int i, j, len;
	char c;

	strip(s);
	len = strln(s);

	i = 0;
	j = len-1;
	while (i < j) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;

		++(i);
		--(j);
	}
	s[len]   = '\n';
	s[len+1] = '\0';

	return s;
}

/* Strip the final newline from a string in place. */
void
strip(char s[]) {
	int i;

	i = 0;
	while (s[i] != '\n') {
		++(i);
	}
	s[i] = '\0';
}

/* Returns the length of a given string. */
int
strln(char s[]) {
	int i;

	i = 0;
	while (s[i] != '\0') {
		++(i);
	}

	return i;
}
