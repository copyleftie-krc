#!/usr/bin/tcc -run
#include <stdio.h>
#define TABSTOP 8

int	empty(char[], int);
int	insert(char[], char, int);
int	flush(char[], int);

int
main() {
	int idx, to_ts;
	char buffer[TABSTOP+1], c;

	empty(buffer, TABSTOP+1);

	idx = 0;
	to_ts = TABSTOP;
	while ((c = getchar()) != EOF) {
		if (c == ' ') {
			if (to_ts == 1) {
				if (idx > 0) {
					idx = empty(buffer, TABSTOP+1);
					putchar('\t');
				} else
					putchar(c);

				to_ts = TABSTOP;
			} else {
				idx = insert(buffer, c, idx);
				--(to_ts);
			}
		} else {
			if (idx > 0)
				idx = flush(buffer, TABSTOP+1);

			if (c == '\n' || to_ts == 1)
				to_ts = TABSTOP;
			else
				--(to_ts);

			putchar(c);
		}
	}

	return 0;
}

/* NULL out a buffer string whose length is known at call time.
 * Returns 0, the only index that makes sense for a flushed buffer */
int
empty(char s[], int len) {
	int i;

	for (i = 0; i < len; ++(i))
		s[i] = '\0';

	return 0;
}

/* Add the given character to the given string at the given index.
 * Returns an incremented index */
int
insert(char s[], char c, int i) {
	s[i] = c;

	return i+1;
}

/* Print and then empty a buffer string whose length is known at call time.
 * Returns 0, the only index that makes sense for a flushed buffer */
int
flush(char s[], int len) {
	printf("%s", s);
	return empty(s, len);
}
