#!/usr/bin/tcc -run
#include <stdio.h>

#define IN  1 /* inside a blank */
#define OUT 0 /* outside a blank */

int
main() {
	int c, state;

	state = OUT;
	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\t') {
			if (state == OUT) {
				state = IN;
				putchar(' ');
			} /* else, noop */
		} else {
			putchar(c);
			if (state == IN)
				state = OUT;
		}
	}

	return 0;
}
