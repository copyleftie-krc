#!/usr/bin/tcc -run
#include <stdio.h>

#define MAXLENGTH 16 /* It is very rare for a word to be so long. */
#define IN         1
#define OUT        0

int
main() {
	int c, i, length, state;
	int nlength[MAXLENGTH]; /* nlength[0] is for words of length 1. */

	for (i = 0; i < MAXLENGTH; ++(i))
		nlength[i] = 0;

	state  = OUT;
	length = 0;
	while ((c = getchar()) != EOF) {
		if (!((c >= '0' && c <= '9')
		    || (c >= 'a' && c <= 'z')
		    || (c >= 'A' && c <= 'Z'))) {
			if (state == IN) {
				state = OUT;
				if (length > 0) {
					++(nlength[length-1]);
					length = 0;
				}
			} /* else, noop */
		} else {
			if (length < MAXLENGTH)
				++(length);
			if (state == OUT)
				state = IN;
		}
	}

	for (i = 0; i < MAXLENGTH; ++(i)) {
		printf("%2d ", i+1);
		for (c = 0; c < nlength[i]; ++(c))
			putchar('*');
		putchar('\n');
	}
	
	return 0;
}