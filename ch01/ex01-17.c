#!/usr/bin/tcc -run
#include <stdio.h>
#define MAXLINE 1024

#define NO  0
#define YES 1

int	getln(char[], int);

int
main() {
	int size, wrap;
	char line[MAXLINE];

	wrap = NO;
	while ((size = getln(line, MAXLINE)) > 0) {
		if (size > 80 || wrap == YES ) {
			printf("%s", line);

			if (line[size-1] != '\n')
				wrap = YES;
			else
				wrap = NO;
		}
	}

	return 0;
}

/* Store the contents of the next line from stdin into the given string. */
int
getln(char s[], int lim) {
	int c, i;

	for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++(i))
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		++(i);
	}
	s[i] = '\0';

	return i;
}
