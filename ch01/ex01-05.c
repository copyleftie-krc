#!/usr/bin/tcc -run
#include <stdio.h>

#define UPPER 300
#define LOWER   0
#define STEP   20

int
main() {
	int f;

	for (f = UPPER; f >= LOWER; f -= STEP)
		printf("%3d %6.1f\n", f, (5.0/9.0)*(f-32));
	
	return 0;
}
