#!/usr/bin/tcc -run
#include <stdio.h>

#define LOWER   0
#define UPPER 300
#define STEP   20

float	c_of_f(float);

int
main() {
	int f;

	for (f = LOWER; f <= UPPER; f += STEP)
		printf("%3d %6.1f\n", f, c_of_f(f));

	return 0;
}

/* Given a temperature in degrees Fahrenheight, returns the equivalent in
 * degrees Celsius. /*
float
c_of_f(float f) {
	return (5.0/9.0) * (f-32.0);
}